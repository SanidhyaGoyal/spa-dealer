import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class SpaDealer {

    public static void main(String args[]) throws IOException {

        BufferedReader input1 = new BufferedReader(new FileReader("/Users/consultadd/Documents/SpaDealer/src/main/resources/input/salesforce_data.csv"));
        BufferedReader input2 = new BufferedReader(new FileReader("/Users/consultadd/Documents/SpaDealer/src/main/resources/input/prod_data.csv"));
        FileWriter fw = new FileWriter("/Users/consultadd/Documents/SpaDealer/src/main/resources/output.csv");
        FileWriter fw2 = new FileWriter("/Users/consultadd/Documents/SpaDealer/src/main/resources/SF.csv");
        FileWriter fw3 = new FileWriter("/Users/consultadd/Documents/SpaDealer/src/main/resources/prod.csv");
        String row="";
        String  row2= "";

        List<String> arr1 = new ArrayList<>();
        List<String> arr2 = new ArrayList<>();

        while ((row = input1.readLine()) != null) {
            String[] data = row.split(",");
            String s= data[5]+","+data[7];
            arr1.add(s);
          }

        while ((row2 = input2.readLine()) != null) {
            String[] data = row2.split(",");
            String s= data[0]+","+data[1];
            arr2.add(s);
        }


        Collections.sort(arr1);
        Collections.sort(arr2);

        for(String s : arr1 ){
            fw2.write(s+"\n");
            //System.out.println(s);
        }
        for(String s : arr2){
            fw3.write(s+"\n");
        }

        int count =0 ;
        for(int i=0;i< arr1.size(); i++ ) {
            for (int j = 0; j < arr2.size(); j++) {
                if (arr1.get(i).equals(arr2.get(j))) {
                    count += 1;
                    fw.write(arr1.get(i) + "\n");
                    //System.out.println(arr1.get(i));
                }
            }
        }
            System.out.println(count);
            fw.close();fw2.close();fw3.close();

    }
}